#include "TutorialApplication.h"

TutorialApplication::TutorialApplication()
{
}

TutorialApplication::~TutorialApplication()
{
}

void TutorialApplication::createScene()
{
  mSceneMgr->setAmbientLight(Ogre::ColourValue(.25, .25, .25));

  Ogre::Light* pointLight = mSceneMgr->createLight("PointLight");
  pointLight->setType(Ogre::Light::LT_POINT);
  pointLight->setPosition(250, 150, 250);
  pointLight->setDiffuseColour(Ogre::ColourValue::White);
  pointLight->setSpecularColour(Ogre::ColourValue::White);

  Ogre::Entity* ninjaEntity = mSceneMgr->createEntity("ninja.mesh");
  Ogre::SceneNode* ninjaNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("NinjaNode");
  ninjaNode->attachObject(ninjaEntity);

  mCamera->lookAt(Ogre::Vector3(250, 150, 250));

}

bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
  bool ret = BaseApplication::frameRenderingQueued(fe);

  if(!processUnbufferedInput(fe))
    return false;

  return ret;
}

bool TutorialApplication::processUnbufferedInput(const Ogre::FrameEvent& fe)
{
  static bool mouseDownLastFrame = false;
  static Ogre::Real toogleTimer = 0.0;
  static Ogre::Real rotate = 0.13;
  static Ogre::Real move = 250;

  bool leftMouseDown = mMouse->getMouseState().buttonDown(OIS::MB_Left);

  if(leftMouseDown && !mouseDownLastFrame)
  {
      Ogre::Light* light = mSceneMgr->getLight("PointLight");
      light->setVisible(!light->isVisible());
  }

  mouseDownLastFrame = leftMouseDown;

  toogleTimer -= fe.timeSinceLastFrame;

  if((toogleTimer < 0) && mMouse->getMouseState().buttonDown(OIS::MB_Right))
  {
      toogleTimer = 0.5;

      Ogre::Light* light = mSceneMgr->getLight("PointLight");
      light->setVisible(!light->isVisible());

  }


  Ogre::Vector3 dirVec = Ogre::Vector3::ZERO;

  if(mKeyboard->isKeyDown(OIS::KC_I))
      dirVec.z -= move;

  if(mKeyboard->isKeyDown(OIS::KC_K))
      dirVec.z += move;

  if(mKeyboard->isKeyDown(OIS::KC_U))
      dirVec.y += move;

  if(mKeyboard->isKeyDown(OIS::KC_O))
      dirVec.y -= move;

  if(mKeyboard->isKeyDown(OIS::KC_J))
  {
      if(mKeyboard->isKeyDown(OIS::KC_LSHIFT))
          mSceneMgr->getSceneNode("NinjaNode")->yaw(Ogre::Degree(5*rotate));
      else
          dirVec.x -= move;
  }
  if(mKeyboard->isKeyDown(OIS::KC_L))
  {
      if(mKeyboard->isKeyDown(OIS::KC_LSHIFT))
          mSceneMgr->getSceneNode("NinjaNode")->yaw(Ogre::Degree(-5*rotate));
      else
          dirVec.x += move;
  }

  mSceneMgr->getSceneNode("NinjaNode")->translate(dirVec * fe.timeSinceLastFrame,
                                                   Ogre::Node::TS_LOCAL);

  return true;

}
