/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _ 
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/                              
      Tutorial Framework
      http://www.ogre3d.org/tikiwiki/
-----------------------------------------------------------------------------
*/
#include "TutorialApplication.h"

//-------------------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//-------------------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//-------------------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    Ogre::Entity* ninjaEntity = mSceneMgr->createEntity("ninja.mesh");
    ninjaEntity->setCastShadows(true);

    mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(ninjaEntity);


    Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);

    Ogre::MeshManager::getSingleton().createPlane(
                "ground",
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                plane,
                1500,1500,20,20,
                true,
                1,5,5,
                Ogre::Vector3::UNIT_Z
                );

    Ogre::Entity* groundEntity = mSceneMgr->createEntity("ground");
    mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(groundEntity);

    groundEntity->setCastShadows(false);
    groundEntity->setMaterialName("Examples/Rockwall");

    mSceneMgr->setAmbientLight(Ogre::ColourValue(0,0,0));
    mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    Ogre::Light* spotLight = mSceneMgr->createLight("SpotLight");

    spotLight->setDiffuseColour(0,0,1);
    spotLight->setSpecularColour(0,0,1);

    spotLight->setType(Ogre::Light::LT_SPOTLIGHT);

    spotLight->setDirection(-1,-1,0);
    spotLight->setPosition(Ogre::Vector3(200,200,0));
    spotLight->setSpotlightRange(Ogre::Degree(35), Ogre::Degree(50));

    Ogre::Light* directionalLight = mSceneMgr->createLight("DirectionalLight");
    directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);

    directionalLight->setDiffuseColour(Ogre::ColourValue(0.4,0,0));
    directionalLight->setSpecularColour(Ogre::ColourValue(0.4,0,0));

    directionalLight->setDirection(Ogre::Vector3(0,-1,1));

    Ogre::Light* pointLight = mSceneMgr->createLight("PointLight");
    pointLight->setType(Ogre::Light::LT_POINT);

    pointLight->setDiffuseColour(.3,.3,.3);
    pointLight->setSpecularColour(.3,.3,.3);

    pointLight->setPosition(0,150,250);

}

void TutorialApplication::createCamera()
{
    mCamera = mSceneMgr->createCamera("PlayerCam");

    mCamera->setPosition(Ogre::Vector3(0,300,500));
    mCamera->lookAt(Ogre::Vector3(0,0,0));

    mCamera->setNearClipDistance(1);

    mCameraMan = new OgreBites::SdkCameraMan(mCamera);

}

void TutorialApplication::createViewports()
{

    Ogre::Viewport* vp = mWindow->addViewport(mCamera);

    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

    mCamera->setAspectRatio(Ogre::Real(vp->getActualWidth() / Ogre::Real(vp->getActualHeight()) ));

}

