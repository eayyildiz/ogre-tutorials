QT       += core
QT       -= gui

TARGET = BasicTutorial3

TEMPLATE = app

unix {
    # You may need to change this include directory
    INCLUDEPATH += /usr/include/OGRE
    INCLUDEPATH += /usr/include/OGRE/Overlay
    INCLUDEPATH += /usr/include/OIS
    CONFIG += link_pkgconfig

}

LIBS *= -lOgreMain -lpthread -lboost_system -lOIS -lOgreOverlay -lOgreTerrain

SOURCES += main.cpp \
    BaseApplication.cpp \
    TutorialApplication.cpp \


HEADERS += \
    BaseApplication.h \
    TutorialApplication.h \

